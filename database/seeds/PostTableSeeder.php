<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'uuid' => Uuid::generate()->string,
            'Image' => 'https://cdn.pixabay.com/photo/2017/10/17/10/05/job-2860035_960_720.jpg',
            'Title' => 'POST A',
            'Text' => 'post',
            'Created_by' => 1,
            'published_by' => 1,
        ]);
        DB::table('posts')->insert([
            'uuid' => Uuid::generate()->string,
            'Image' => 'https://cdn.pixabay.com/photo/2017/10/17/10/05/job-2860035_960_720.jpg',
            'Title' => 'POST B',
            'Text' => 'post',
            'Created_by' => 1,
            'published_by' => 1,
        ]);
        DB::table('posts')->insert([
            'uuid' => Uuid::generate()->string,
            'Image' => 'https://cdn.pixabay.com/photo/2017/10/17/10/05/job-2860035_960_720.jpg',
            'Title' => 'POST C',
            'Text' => 'post',
            'Created_by' => 1,
            'published_by' => 1,
        ]);
        DB::table('posts')->insert([
            'uuid' => Uuid::generate()->string,
            'Image' => 'https://cdn.pixabay.com/photo/2017/10/17/10/05/job-2860035_960_720.jpg',
            'Title' => 'POST D',
            'Text' => 'post',
            'Created_by' => 1,
            'published_by' => 1,
        ]);
        DB::table('posts')->insert([
            'uuid' => Uuid::generate()->string,
            'Image' => 'https://cdn.pixabay.com/photo/2017/10/17/10/05/job-2860035_960_720.jpg',
            'Title' => 'POST E',
            'Text' => 'post',
            'Created_by' => 1,
            'published_by' => 1,
        ]);
        DB::table('posts')->insert([
            'uuid' => Uuid::generate()->string,
            'Image' => 'https://cdn.pixabay.com/photo/2017/10/17/10/05/job-2860035_960_720.jpg',
            'Title' => 'POST F',
            'Text' => 'post',
            'Created_by' => 1,
            'published_by' => 1,
        ]);
    }
}
