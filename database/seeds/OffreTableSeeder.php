<?php

use Illuminate\Database\Seeder;

class OffreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('offres')->insert([
            'uuid' => Uuid::generate()->string,
            // 'Image' => 'https://cdn.pixabay.com/photo/2017/10/17/10/05/job-2860035_960_720.jpg'
            'Title' => 'Offre A',
            'Text' => 'offre',
            'Created_by' => 1,
            'published_by' => 1,
            'secteur_id' => 1,
        ]);
        DB::table('offres')->insert([
            'uuid' => Uuid::generate()->string,
            // 'Image' => 'https://cdn.pixabay.com/photo/2017/10/17/10/05/job-2860035_960_720.jpg'
            'Title' => 'Offre B',
            'Text' => 'offre',
            'Created_by' => 1,
            'published_by' => 1,
            'secteur_id' => 2,
        ]);
        DB::table('offres')->insert([
            'uuid' => Uuid::generate()->string,
            // 'Image' => 'https://cdn.pixabay.com/photo/2017/10/17/10/05/job-2860035_960_720.jpg'
            'Title' => 'Offre C',
            'Text' => 'offre',
            'Created_by' => 1,
            'published_by' => 1,
            'secteur_id' => 3,
        ]);
    }
}
