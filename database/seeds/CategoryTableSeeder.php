<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'uuid' => Uuid::generate()->string,
            'Title' => 'Category A',
            'Description' => 'tout a propos Category A',
        ]);
        DB::table('categories')->insert([
            'uuid' => Uuid::generate()->string,
            'Title' => 'Category B',
            'Description' => 'tout a propos Category B',
        ]);
        DB::table('categories')->insert([
            'uuid' => Uuid::generate()->string,
            'Title' => 'Category C',
            'Description' => 'tout a propos Category C',
        ]);
    }
}
