<?php

use Illuminate\Database\Seeder;

class SecteurTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('secteurs')->insert([
            'uuid' => Uuid::generate()->string,
            'Title' => 'INFORMATIQUE',
            'Description' => 'tout a propos informatique',
        ]);
        DB::table('secteurs')->insert([
            'uuid' => Uuid::generate()->string,
            'Title' => 'MARKETING',
            'Description' => 'tout a propos MARKETING',
        ]);
        DB::table('secteurs')->insert([
            'uuid' => Uuid::generate()->string,
            'Title' => 'COMMUNICATION',
            'Description' => 'tout a propos COMMUNICATION',
        ]);
    }
}
