<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Get Posts
Route::get('getPosts', 'BlogController@getPosts');

// Get Offres
Route::get('getOffres', 'CarriereController@getOffres');

// Get Categories
Route::get('getCategories', 'BlogController@getCategories');

// Filter Posts
Route::post('filterPosts', 'BlogController@filterPosts');

// Get Secteurs
Route::get('getSecteurs', 'CarriereController@getSecteurs');

// Filter Offres
Route::post('filterOffres', 'CarriereController@filterOffres');