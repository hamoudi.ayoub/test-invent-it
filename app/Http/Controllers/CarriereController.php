<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offre;
use App\Secteur;
class CarriereController extends Controller
{
    /**
     * get offre
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getOffres(Request $request)
    {

        $offres = Offre::with('secteur')->get();

        if ($offres) {
            return response([
                'data' => [
                    'offres' => $offres
                ]
            ]);
        }else{
            return response([
                'data' => [
                    'offres' => []
                ]
            ]);    
        }
    }
    /**
     * get secteur
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getSecteurs(Request $request)
    {

        $secteurs = Secteur::all()->pluck('Title');

        if ($secteurs) {
            return response([
                'data' => [
                    'secteurs' => $secteurs
                ]
            ]);
        }else{
            return response([
                'data' => [
                    'secteurs' => []
                ]
            ]);    
        }
    }
    /**
     * filter offre
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function filterOffres(Request $request)
    {
        if(count($request->get('filter')) > 0){
            $offres = Offre::whereHas('secteur', function($q) use($request)
            {
                $q->whereIn('Title', $request->get('filter'));
            
            })->with('secteur')->get();
    
            if ($offres) {
                return response([
                    'data' => [
                        'offres' => $offres
                    ]
                ]);
            }else{
                return response([
                    'offres' => [
                        'offres' => []
                    ]
                ]);    
            }
        }else{
            $offres = Offre::with('secteur')->get();

            if ($offres) {
                return response([
                    'data' => [
                        'offres' => $offres
                    ]
                ]);
            }else{
                return response([
                    'data' => [
                        'offres' => []
                    ]
                ]);    
            } 
        }

    }
}
