<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Category;
class BlogController extends Controller
{
    /**
     * get post
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getPosts(Request $request)
    {

        $posts = Post::with('categories')->get();

        if ($posts) {
            return response([
                'data' => [
                    'posts' => $posts
                ]
            ]);
        }else{
            return response([
                'data' => [
                    'posts' => []
                ]
            ]);    
        }
    }
    /**
     * get category
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getCategories(Request $request)
    {

        $categories = Category::all()->pluck('Title');

        if ($categories) {
            return response([
                'data' => [
                    'categories' => $categories
                ]
            ]);
        }else{
            return response([
                'data' => [
                    'categories' => []
                ]
            ]);    
        }
    }
    /**
     * filter post
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function filterPosts(Request $request)
    {
        if(count($request->get('filter')) > 0){
            $posts = Post::whereHas('categories', function($q) use($request)
            {
                $q->whereIn('Title', $request->get('filter'));
            
            })->with('categories')->get();
    
            if ($posts) {
                return response([
                    'data' => [
                        'posts' => $posts
                    ]
                ]);
            }else{
                return response([
                    'data' => [
                        'posts' => []
                    ]
                ]);    
            }
        }else{
            $posts = Post::with('categories')->get();

            if ($posts) {
                return response([
                    'data' => [
                        'posts' => $posts
                    ]
                ]);
            }else{
                return response([
                    'data' => [
                        'posts' => []
                    ]
                ]);    
            } 
        }

    }
}
