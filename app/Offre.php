<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offre extends Model
{
    public function secteur()
    {
        return $this->hasOne('App\Secteur', 'id');
    }
}
