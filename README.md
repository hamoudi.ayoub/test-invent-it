# Prérequis

  - Npm
  - Composer

# Installation

1. INSTALL PROJECT
   - Clone projet : git clone https://gitlab.com/hamoudi.ayoub/test-invent-it.git
   - Go to directory
   - Execute : npm install
   - Execute : composer install

2. SET UP CONNECTION DATABASE
   - Copie contenu du fichier .env.example et cree un fichier .env et coller le contenu
   - Creer database dans votre local nommee TEST
   - Changer les acces dans le fichier .env

3. SET UP MIGRATION
   - execute : php artisan migrate      ( pour creer les tables dans votre database TEST)

4. Alimentation DATABASE
   - Execute : php artisan db:seed --class=UsersTableSeeder   ( Creer un utilisateur dans la table users)
   - Execute : php artisan db:seed --class=SecteurTableSeeder ( Creer les secteurs dans la table secteurs)
   - Execute : php artisan db:seed --class=CategoryTableSeeder ( Creer les categories dans la table categories)
   - Execute : php artisan db:seed --class=OffreTableSeeder ( Creer les offres dans la table offres)
   - Execute : php artisan db:seed --class=PostTableSeeder ( Creer les posts dans la table posts)
   - Execute : php artisan db:seed --class=PostCategoryTableSeeder ( Creer les relations post category )

5. START SERVER 
   - Execute : npm run dev
   - Execute : php artisan serve
   - Go to ( /blog) pour affichage posts avec filter
   - Go to ( /carriere) pour affichage offres avec filter